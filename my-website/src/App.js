/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react";
import "./App.css";
import Particles from "react-particles-js";
import { particlesConfing } from "./particles";
import { Element, scroller } from "react-scroll";
import styled, { keyframes } from "styled-components";
import { CustomizedButton } from "./components/CustomizedButton";
import design from "./assets/images/design.png";
import development from "./assets/images/development.png";
import launch from "./assets/images/launch.png";
import { Loading } from "./components/Loading";
import "font-awesome/css/font-awesome.min.css";

const showTitleCard = keyframes`
  from {
    opacity: 0;
    transform: translateY(-100px);
  }

  to {
    opacity: 1;
    transform:  translateY(0);
  }
`;

const TitlePageContainer = styled.div`
  background-color: black;
  height: 100vh;
`;

const TitleCardWrapper = styled.div`
  color: white;
  display: flex;
  align-items: center;
  justify-content: center;
  height: 100%;
  animation: ${showTitleCard} 2s;
`;

const TitleCardContentContainer = styled.div`
  z-index: 10;
  display: flex;
  align-items: center;
  flex-direction: column;
  justify-content: center;
  background-color: transparent;
  color: black;
  padding: 40px;
  border-radius: 4px;
`;

const handleScrollToProjects = () => {
  scroller.scrollTo("myProjects", {
    duration: 2000,
    delay: 50,
    smooth: true,
  });
};

const handleScrollToBuildProcess = () => {
  scroller.scrollTo("buildProcess", {
    duration: 2000,
    delay: 50,
    smooth: true,
  });
};

const App = () => {
  const [isLoading, setIsLoading] = React.useState(true);
  React.useEffect(() => {
    setTimeout(() => {
      setIsLoading(false);
    }, 2000);
  }, []);
  return (
    <>
      {isLoading ? (
        <Loading />
      ) : (
        <>
          <TitlePageContainer>
            <TitleCardWrapper>
              <TitleCardContentContainer>
                <div className="titleLogo">M T C W</div>
                <CustomizedButton
                  onClick={handleScrollToProjects}
                  name={"projects"}
                  backgroundColor="#21ebff"
                />
                <CustomizedButton
                  onClick={handleScrollToBuildProcess}
                  name={"BUILD PROCESS"}
                  backgroundColor="#21ebff"
                />
              </TitleCardContentContainer>
            </TitleCardWrapper>
            <Particles
              params={particlesConfing}
              style={{
                position: "absolute",
                width: "100%",
                height: "100%",
                top: 0,
              }}
            />
          </TitlePageContainer>
          <Element name="myProjects" />

          <div className="pageContainer">
            <div className="box">
              <div className="glass" />
              <CustomizedButton
                href="https://suits-lp.netlify.app/"
                name="WEBSITE"
                backgroundColor={"#21ebff"}
              />

              <div className="content">
                <h2>SUITS-LP</h2>

                <p>
                  Fan landing page - currently in build. Will complete this
                  description when I finish the site
                </p>
              </div>
            </div>
            <div className="box">
              <div className="glass" />
              <CustomizedButton
                href="https://frcomps.netlify.app/"
                name="WEBSITE"
                backgroundColor={"#21ebff"}
              />

              <div className="content">
                <h2>FRCOMPS</h2>

                <p>
                  Website with many free reusable React components, based on
                  styled-components. Code included
                </p>
              </div>
            </div>
            <div className="box">
              <div className="glass" />
              <CustomizedButton
                href="https://countrapp.netlify.app/"
                name="WEBSITE"
                backgroundColor="#21ebff"
              />
              <div className="content">
                <h2>COUNTRAPP</h2>
                <p>
                  Check details about every country. Timezones, alpha codes,
                  phone codes and more...
                </p>
              </div>
            </div>
          </div>
          <Element name="buildProcess" />
          <div className="tabsPageContainer">
            <div className="tabsContainer">
              <div className="tabsCard">
                <div className="imgBx">
                  <img src={design} alt="" />
                  <h3>Design</h3>
                </div>
                <div className="tabsContent">
                  <p>
                    Kiedyś dodam tu coś naprawdę mądrego. Wpierw muszę to
                    wymyśleć także jak to czytasz to miłego dnia :)
                  </p>
                </div>
              </div>
              <div className="tabsCard">
                <div className="imgBx">
                  <img src={development} alt="" />
                  <h3>Development</h3>
                </div>
                <div className="tabsContent">
                  <p>
                    Kiedyś dodam tu coś naprawdę mądrego. Wpierw muszę to
                    wymyśleć także jak to czytasz to miłego dnia :)
                  </p>
                </div>
              </div>
              <div className="tabsCard">
                <div className="imgBx">
                  <img src={launch} alt="" />
                  <h3>Launch</h3>
                </div>
                <div className="tabsContent">
                  <p>
                    Kiedyś dodam tu coś naprawdę mądrego. Wpierw muszę to
                    wymyśleć także jak to czytasz to miłego dnia :)
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div
            style={{
              position: "relative",
              height: "250px",
              width: "100%",
              display: "flex",
              flexFlow: "row-wrap",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <ul>
              <li>
                <a
                  href="https://www.linkedin.com/in/mateuszcwiklinski/"
                  target="_blank"
                >
                  <span></span>
                  <span></span>
                  <span></span>
                  <span></span>
                  <span className="fa fa-linkedin"></span>
                </a>
              </li>
            </ul>
          </div>
        </>
      )}
    </>
  );
};

export default App;
