import React from "react";
import styled from "styled-components";
//#21ebff - bckgrnd
const ButtonContainer = styled.a`
  position: relative;
  padding: 10px 30px;
  margin: 15px 15px;
  color: ${(props) => props.backgroundColor};
  text-decoration: none;
  text-transform: uppercase;
  letter-spacing: 2px;
  @media (max-width: 768px) {
    font-size: 15px;
  }
  font-size: 25px;
  transition: 0.5s;
  background-color: transparent;
  overflow: hidden;
  cursor: pointer;
  &::before {
    content: "";
    position: absolute;
    top: 0;
    left: 0;
    width: 10px;
    height: 10px;
    border-top: 2px solid ${(props) => props.backgroundColor};
    border-left: 2px solid ${(props) => props.backgroundColor};
    transition: 0.5s;
  }
  &:hover::before {
    width: 100%;
    height: 100%;
  }
  &::after {
    content: "";
    position: absolute;
    bottom: 0;
    right: 0;
    width: 10px;
    height: 10px;
    border-bottom: 2px solid ${(props) => props.backgroundColor};
    border-right: 2px solid ${(props) => props.backgroundColor};
    transition: 0.5s;
  }
  &:hover::after {
    width: 100%;
    height: 100%;
  }

  &:hover {
    background: ${(props) => props.backgroundColor};
    color: #111;
    box-shadow: 0 0 50px ${(props) => props.backgroundColor};
  }
`;

export const CustomizedButton = ({ name, href, backgroundColor, onClick }) => (
  <ButtonContainer
    onClick={onClick}
    href={href}
    className="lightningButton"
    backgroundColor={backgroundColor}
    target="_blank"
  >
    {name}
  </ButtonContainer>
);
